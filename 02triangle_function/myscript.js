/*
  p5 dom
*/

var angle = 0;

function setup(){
  createCanvas(600, 600);

  
  slider = createSlider(0, 720, 0);
  slider.position(10,20);
  
}

function draw(){
  background("skyblue");
  ellipseMode(CENTER);
  stroke(255)
  noFill();
  ellipse(width/2, height/2, 500, 500);

  r = slider.value();
  ellipseMode(CENTER);
  textSize(20);
  
  
  // 縦と横の線を書く
  line(width/2, 0, width/2, height)
  line(0, height/2,width, height/2)
  
  // 円に沿う、円を書く
  ellipseMode(CENTER);
  fill(255)
  ellipse(cos(radians(r))*250+width/2, sin(radians(r))*250+height/2, 10, 10);

  // 線を書く
  strokeWeight(2);
  stroke('pink')
  line(width/2, height/2 ,cos(radians(r))*250+width/2, sin(radians(r))*250+height/2)  
  line(cos(radians(r))*250+width/2, height/2, cos(radians(r))*250+width/2, sin(radians(r))*250+height/2)  
  line(width/2, sin(radians(r))*250+height/2, cos(radians(r))*250+width/2, sin(radians(r))*250+height/2)  

  // 円弧を書く
  fill(255)
  arc(width/2 , height/2, 250, 250, 0, radians(r))
  
  noStroke();
  
  fill(64);
  text("度数: "+ r%360,10,50);
  text("ラディアン: " + radians(r),10,75);
  text("cos :" +cos(radians(r)),10,100);
  text("sin :" +sin(radians(r)),10,125);
  text("x = cos * 半径 + 横位置: " + sin(radians(r)) * 250 + 300,10,150)
  text("y = sin * 半径 + 縦位置: " + cos(radians(r)) * 250 + 300,10,175)

  textSize(50);
  text("y", width/2 + 10, height - 15)
  text("x", width - 25, height/2 - 10)
  
}